<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use phpseclib3\Net\SFTP;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Request;



class InstallerController extends AbstractController
{
    /**
     * @Route("/installer/{path}", name="installer", requirements={"path" = ".+"})
     */
    public function index($path): Response
    {

        $pathInstaller=str_replace("/", chr(92), $path).chr(92);
        $path = $pathInstaller."dts".chr(92);
        $fromDTS=$path."fromDTS";
        $toDTS=$path."toDTS";
        $temp=$path."temp";
        $logs=$path."logs";
        $backup=$path."backup".chr(92)."toDTS";
        return $this->render('installer/installer.html.twig', [
            'fromDTS' => $fromDTS,
            'toDTS' => $toDTS,
            'temp' => $temp,
            'logs' => $logs,
            'backup' => $backup,
            'path'=>$path,
            'pathInstaller'=>$pathInstaller,
        ]);
    }
    
     /**
     *
     * @Route("/loadfolder/{user}/{password}", name="loadfolder", options={"expose"=true}, methods={"POST"})
     */
    public function loadFolder($user,$password){

        $sftp = new SFTP('172.18.2.205');
        
        $sftp_login = $sftp->login($user, $password);
        
        if($sftp_login) {
           
            $sftp->chdir('todts');
            $toSftp=$sftp->nlist();
            foreach($toSftp  as $key => $value){
                if($value=='.' || $value=='..'){
                    unset($toSftp[$key]);
                }
                if (strlen(strstr($value,'.'))>0) {
                    unset($toSftp[$key]);
                }
            }
            
            $sftp->chdir('../fromdts');
            $fromSftp=$sftp->nlist();
            foreach($fromSftp  as $key => $value){
                if($value=='.' || $value=='..'){
                    unset($fromSftp[$key]);
                }
                if (strlen(strstr($value,'.'))>0) {
                    unset($fromSftp[$key]);
                }
            }
            
            $sftpArray['To']=array_values($toSftp);
            $sftpArray['From']=array_values($fromSftp);
            
            
        }else{
            
            throw new \Exception('Cannot login into your server !');
            
        }
        return new JsonResponse($sftpArray); 
    }
    
    /**
     *
     * @Route("/generateScript/", name="generate_script", methods={"POST"})
     */
    public function generateScript(Request $request){
        
        $user=$request->get('user');
        $password=$request->get('password');
        $jsonTo=$request->get('jsonTo');
        $jsonFrom=$request->get('jsonFrom');
        $fromDtsLocal=$request->get('fromdts');
        $toDtsLocal=$request->get('todts');
        $backup=$request->get('backup');
        $uploadSubdirs=$request->get('subDirs');
        $qtySubdir=$request->get('qtySubdirs'); 
        $downloadSubdirs=$request->get('subDirsRemote');
        $qtySubdirRemote=$request->get('qtySubdirsRemote'); 
        $pathlocaldts=$request->get('pathlocal'); 
        $path=substr($request->get('pathInstaller'), 0, -1);
        $logs=$pathlocaldts."logs";
        $pathInst = substr($pathlocaldts, 0, -1);
        
        if($qtySubdir>0){
            for ($i = 1; $i <= $qtySubdir; $i++) {
                $dirLocalTo=$request->get('subDir'.$i.'2');  
                if($jsonTo!=""){
                    $dirRemoteTo="todts/".$request->get('subDir'.$i.'4')."/";
                }else{
                    $subdirs= explode('/', $request->get('subDir'.$i.'3'));
                    $dirRemoteTo=$subdirs[1]."/";
                }
                $arrayTo[$i]['source']=$toDtsLocal.chr(92).$dirLocalTo.chr(92).'*';
                $arrayTo[$i]['destination']=$dirRemoteTo;
                $arrayTo[$i]['backup']=$backup.chr(92).$dirLocalTo;
            }
        }else{
            $arrayTo[1]['source']=$toDtsLocal.chr(92).'*';
            $arrayTo[1]['destination']="todts/";
            $arrayTo[1]['backup']=$backup;
        }

        if($qtySubdirRemote>0){
            for ($i = 1; $i <= $qtySubdirRemote; $i++) {
                $dirLocalFrom=$request->get('subDirRemote'.$i.'2'); 

                if($jsonFrom!=""){
                    $dirRemoteFrom="fromdts/".$request->get('subDirRemote'.$i.'4').'/*';
                    $dirRemoteBackup=$request->get('subDirRemote'.$i.'4')."/";
                }else{
                    $subdirs= explode('/', $request->get('subDirRemote'.$i.'3'));
                    $dirRemoteFrom=$subdirs[1]."/*";
                    $dirRemoteBackup="";
                }
                $arrayFrom[$i]['destination']=$fromDtsLocal.chr(92).$dirLocalFrom.chr(92);
                $arrayFrom[$i]['source']=$dirRemoteFrom;
                $arrayFrom[$i]['backup']=".system/fromdts_bak/".$dirRemoteBackup;
            }
        }else{
            $arrayFrom[1]['destination']=$fromDtsLocal.chr(92);
            $arrayFrom[1]['source']="fromdts/*";
            $arrayFrom[1]['backup']=".system/fromdts_bak/";
        }
        
  
        
$outfile = <<<EOD
function Start-Main{
    if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"\$PSCommandPath`"" -Verb RunAs; exit }    
    \$taskName = "SFTPTask"
    \$taskExist = Get-ScheduledTask | Where-Object {\$_.TaskName -like \$taskName }

    if(\$taskExist) {
       Filetransfer-WinSCP
    } else {
       Create-ScheduledTask
       Filetransfer-WinSCP
    }
}

function Create-ScheduledTask{
    #\$time=(Get-Date).AddMinutes(5).ToString("HH:mm")
    \$TaskAction1 = New-ScheduledTaskAction -Execute 'PowerShell.exe' -Argument "-ExecutionPolicy Bypass -File ""$pathInst\ScheduledTask\dts_sftp_config.ps1"""
    \$TaskUserName = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
    \$TaskSettings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
    \$TaskTrigger  = New-ScheduledTaskTrigger -Daily -At 12:00
    \$Task = New-ScheduledTask -Action \$TaskAction1 -Principal \$TaskUserName -Trigger \$TaskTrigger -Settings \$TaskSettings
    Register-ScheduledTask "SFTPTask" -InputObject \$Task -Force
    \$Task = Get-ScheduledTask -TaskName "SFTPTask"
    \$Task.Triggers.Repetition.Duration = ""
    \$Task.Triggers.Repetition.Interval = "PT15M"
    \$Task | Set-ScheduledTask -User "NT AUTHORITY\SYSTEM"
}
        
function Filetransfer-WinSCP{

    try{
        Add-Type -Path "$path\WinSCPnet.dll"
        \$ServerName="sftp.datatrans-inc.com"
        \$Username="$user"
        \$password="$password"
        \$sessionOptions = New-Object WinSCP.SessionOptions -Property @{ 
            Protocol = [WinSCP.Protocol]::Sftp
            HostName = \$ServerName
            UserName = \$Username
            Password = \$password
            SshHostKeyFingerprint = "ssh-rsa 1024 EbnO4btS1fS2LDYkEUKnsA5i1AYChovO1XyJABhLJaM="
        }
        \$session = New-Object WinSCP.Session
        \$session.SessionLogPath="$logs\sftp.log"
        \$session.DebugLogPath="$logs\sftpdebug.log"
        \$session.Open(\$sessionOptions)
        \$transferOptions = New-Object WinSCP.TransferOptions
        \$transferOptions.TransferMode = [WinSCP.TransferMode]::Binary

EOD;
foreach ($arrayTo as $to){
    $toSourceAux=substr($to['source'], 0, -2);
$outfile .= <<<EOD
        \$SourcePath="$to[source]"
        \$SourcePathAux="$toSourceAux"
        if (-not (test-path \$SourcePathAux)) 
        {
            New-Item \$SourcePathAux -itemType Directory
        }
        \$DestinationPath="$to[destination]"
        
        \$transferResult=\$session.PutFiles("\$SourcePath", "\$DestinationPath", \$False, \$transferOptions)
	
	foreach (\$transfer in \$transferResult.Transfers)
        {
            if (\$transfer.Error -eq \$Null)
            {
                \$backupPath="$to[backup]"

		if (-not (test-path \$backupPath)) 
        	{
            		New-Item \$backupPath -itemType Directory
        	}

		\$fileNameBackup=\$transfer.FileName+"_"+(Get-Date -Format "yyyyMMddhhmmss")
	
		rename-item \$transfer.FileName \$fileNameBackup 
		Move-item \$fileNameBackup \$backupPath
            }
        }

EOD;
}

foreach ($arrayFrom as $from){
    $outfile .= <<<EOD
        \$SourcePath="$from[source]"
        \$DestinationPath="$from[destination]"
        if (-not (test-path \$DestinationPath)) 
        {
            New-Item \$DestinationPath -itemType Directory
        }
        \$transferResult=\$session.GetFiles("\$SourcePath", "\$DestinationPath", \$False, \$transferOptions)
        
        foreach (\$transfer in \$transferResult.Transfers)
        {
            if (\$transfer.Error -eq \$Null)
            {
                \$severBackup="$from[backup]"
                if (-not (\$session.FileExists(\$severBackup)))
                {
                    \$session.CreateDirectory(\$severBackup)
                }
                \$newName =\$severBackup+[IO.Path]::GetFileName(\$transfer.FileName)+"_"+(Get-Date -Format "yyyyMMddhhmmss")
                \$session.MoveFile(\$transfer.FileName, \$newName)
            }
        }

EOD;
}

$outfile .= <<<EOD
    }
    finally
    {
        \$session.Dispose()
    }
    exit 0
    catch
    {
        Write-Host "Error: $(\$_Exception.Message)"
        exit 1
    }
}
Start-Main
EOD;
        
        $filename = "dts_sftp_config.ps1";
        
        // Return a response with a specific content
        $response = new Response($outfile);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);

        // Dispatch request
        return $response;
        
    }

}

