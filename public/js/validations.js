function validacion(){
    var user = document.getElementById("user").value;
    var pass = document.getElementById("password").value;
    var qtySubdirs = document.getElementById("qtySubdirs").value;
    var subDirs = document.getElementById('subDirs').checked;
    var qtySubdirsRemote = document.getElementById("qtySubdirsRemote").value;
    var subDirsRemote = document.getElementById('subDirsRemote').checked;

    if( user == null || user.length == 0 || /^\s+$/.test(user) ) {
      alert ("User is required");
      return false;
    }
    if( pass == null || pass.length == 0 || /^\s+$/.test(pass) ) {
      alert ("Password is required.")
      return false;
    }

    if( /([a-zA-Z]:)?(\\[a-zA-Z0-9_-]+)+\\?/.test(user) || /^https?:\/\/[\w\-]+(\.[\w\-]+)+[/#?]?.*$/.test(user) || /^([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)\.([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)\.([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)\.([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)$/.test(user)) {
      alert("User is invalid.");
      return false;
    }
    if( /([a-zA-Z]:)?(\\[a-zA-Z0-9_-]+)+\\?/.test(pass) || /^https?:\/\/[\w\-]+(\.[\w\-]+)+[/#?]?.*$/.test(pass) || /^([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)\.([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)\.([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)\.([0-9]|[0-9][0-9]|[0-9][0-9][0-9]?)$/.test(pass)) {
      alert("Password is invalid.");
      return false;
    }
    
    if(subDirs && (qtySubdirs=="" || qtySubdirs==0)){
      alert("If do you have subdirs, you should indicate how many subdirs need");
      return false;
    }
    
    if(subDirsRemote && (qtySubdirsRemote=="" || qtySubdirsRemote==0)){
      alert("If do you have subdirsRemote, you should indicate how many subdirsRemote need");
      return false;
    }
    
    if(qtySubdirs>12){
        alert("Subdirs quantity should be less to 12");
        return false;
    }
    
    if(qtySubdirsRemote>12){
        alert("SubdirsRemote quantity should be less to 12");
        return false;
    }
    
    if(qtySubdirs>0){
        for (var i = 1; i <= qtySubdirs; i++) {
            var subDirsinput = document.getElementById('subDir'+i+'2').value;
            if(subDirsinput==""){
                alert("Please verify information in subDir inputs.");
                return false;
            }
            if((/([a-zA-Z]:)?(\\[a-zA-Z0-9_-]+)+\\?/.test(subDirsinput))){
            alert("Please indicate subdir name.");
            return false;
            }
            if(document.getElementById('subDir'+i+'4')){
                var selectDirs = document.getElementById('subDir'+i+'4').value;
                if(selectDirs=='Select'){
                   alert("Please select SubDirs"); 
                   return false;
                }
            }
        }
    }
    
    if(qtySubdirsRemote>0){
        for (var i = 1; i <= qtySubdirsRemote; i++) {
            var subDirsinputRemote = document.getElementById('subDirRemote'+i+'2').value;
            if(subDirsinputRemote==""){
                alert("Please verify information in subDir inputs.");
                return false;
            }
            if((/([a-zA-Z]:)?(\\[a-zA-Z0-9_-]+)+\\?/.test(subDirsinputRemote))){
                alert("Please indicate subdir name.");
                return false;
            }
            if(document.getElementById('subDirRemote'+i+'4')){
                var selectDirs = document.getElementById('subDirRemote'+i+'4').value;
                if(selectDirs=='Select'){
                   alert("Please select SubDirs"); 
                   return false;
                }
            }
        }
    }
    
   
}

function CreateInputs(server, e){

    if(server=="local"){     
        var qtySubdirs = document.getElementById("qtySubdirs").value;
        var subDirs = document.getElementById('subDirs').checked; 
        var classname='subDir';
        var path = document.getElementById("todts").value;
        var arrow="right";
        var json=document.getElementById("jsonTo").value;
        var qtyAux='qtySubdirs';
        var divSubDir = document.getElementById("LocalSubDir");
        
    }else if(server=="remote"){
        var qtySubdirs = document.getElementById("qtySubdirsRemote").value;
        var subDirs = document.getElementById('subDirsRemote').checked;
        var classname='subDirRemote';
        var path = document.getElementById("fromdts").value;
        var arrow="left";
        var json=document.getElementById("jsonFrom").value;
        var qtyAux='qtySubdirsRemote';
        var divSubDir = document.getElementById("RemoteSubDir");
    }

    if(!/^[0-9]+$/.test(qtySubdirs)){
        document.getElementById(qtyAux).value="";
        return false;
    }

    divSubDir.style.display = "block";


    var qtyNewsubdirs = document.getElementsByClassName(classname).length;
    var newMargin=qtySubdirs * 70;
        
    if(json!==""){
        var dinamicDir = json.split(',');
        dinamicDir.unshift('Select');
        var dinamicLength=dinamicDir.length;
        var fields=qtyNewsubdirs/2;
        if(qtySubdirs>(dinamicLength-1)){ 
            document.getElementById(qtyAux).value="";
            alert('You can add '+(dinamicLength-1)+' subdirs.');
            return false;
        }
        
    }else{
        var dinamicLength=0;
        var fields=qtyNewsubdirs;
        if(qtySubdirs>1){
            document.getElementById(qtyAux).value="";
            alert('You can add '+1+' subdirs.');
            return false;
        }
    }

    if(qtySubdirs>12){
        alert("Subdirs quantity should be less to 12");
        return false;
    }
    
    if(subDirs && (qtySubdirs=="" || qtySubdirs=="" || qtySubdirs=="0")){
      alert("If do you have subdirs, you should indicate how many subdirs need.");
      return false;
      
    }else{
        
        if(server=="local"){     
            var newSubdirs1 = document.getElementById("localFromdir");
            var newSubdirs2 = document.getElementById("localFrom");
            var newSubdirs3 = document.getElementById("remoteTodir");
            var newSubdirs4 = document.getElementById("remoteTo");
            var newSubdirs5 = document.getElementById("label");
            var pathName="==>                /todts/";
            
   
            
        }else if(server=="remote"){
            var newSubdirs1 = document.getElementById("remoteFromdir");
            var newSubdirs2 = document.getElementById("remoteFrom");
            var newSubdirs3 = document.getElementById("localTodir");
            var newSubdirs4 = document.getElementById("localTo");
            var newSubdirs5 = document.getElementById("label2");
            var pathName="<==            /fromdts/";
            
            
        }
             
        if(qtyNewsubdirs!=0){
            for (var i = 1; i <=fields; i++) {
                var olddir1 = document.getElementById(classname+i+'1');
                var olddir2 = document.getElementById(classname+i+'2');
                var olddir3 = document.getElementById(classname+i+'3');
                
                newSubdirs1.removeChild(olddir1);
                newSubdirs2.removeChild(olddir2);
                newSubdirs3.removeChild(olddir3);
                
                if(dinamicLength!=0){
                    var olddir4 = document.getElementById(classname+i+'4');
                    newSubdirs4.removeChild(olddir4);
                }  
            }
        }
        
        for (var i = 1; i <= qtySubdirs; i++) {
              
            var pathFrom = document.createElement("INPUT");
                pathFrom.type = 'text';
                pathFrom.name=classname+i+'1';
                pathFrom.id=classname+i+'1';
                pathFrom.className='subDirpath';
                pathFrom.value=path;
                pathFrom.readOnly=true;
            newSubdirs1.appendChild(pathFrom);

            newSubdirs5.style.marginTop= newMargin+"px";
            
           
            var subDir = document.createElement("INPUT");
                subDir.type = 'text';
                subDir.name=classname+i+'2';
                subDir.id=classname+i+'2';
                subDir.className=classname;
            newSubdirs2.appendChild(subDir);
            
            var pathTo = document.createElement("INPUT");
                pathTo.type = 'text';
                pathTo.name=classname+i+'3';
                pathTo.id=classname+i+'3';
                pathTo.className='subDirpath1';
                pathTo.readOnly=true;
                pathTo.value=pathName;
            newSubdirs3.appendChild(pathTo);
            
            if(dinamicLength>0){
            
                var subDirTo = document.createElement("SELECT");
            
                for (var j = 0; j <dinamicLength ; j++) {
                    var option = document.createElement("option");
                        option.text =dinamicDir[j]; 
                        option.value =dinamicDir[j]; 
                        subDirTo.add(option);
                        subDirTo.name=classname+i+'4';
                        subDirTo.id=classname+i+'4';
                        subDirTo.className=classname;
                    newSubdirs4.appendChild(subDirTo); 
                }
            }
        }   
              
    }
    
}

function showInputs(server){
   
    
    if(server=="local"){
        
        var subDirs = document.getElementById('subDirs').checked; 
        var divqty = document.getElementById("divqty");
        var qtySubdirs = document.getElementById("qtySubdirs");
        var divlabel = document.getElementById("label");
        var divSubDir = document.getElementById("LocalSubDir");
        
    }else if(server=="remote"){

        var subDirs = document.getElementById('subDirsRemote').checked; 
        var divqty = document.getElementById("divqtyremote");
        var qtySubdirs = document.getElementById("qtySubdirsRemote");
        var divlabel = document.getElementById("label2");
        var divSubDir = document.getElementById("RemoteSubDir");
    }
    
    if(subDirs){
        divqty.style.display = "block";
        
    }else{
        qtySubdirs.value="";
        divqty.style.display = "none";
        divSubDir.style.display = "none";
        divlabel.style.marginTop= "20px";
        CreateInputs(server);
    }
}

