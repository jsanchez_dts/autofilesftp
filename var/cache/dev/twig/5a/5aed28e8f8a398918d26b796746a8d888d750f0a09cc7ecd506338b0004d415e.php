<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* installer/installer.html.twig */
class __TwigTemplate_904df6a41a86343f6f2e4bf80a4b9bb84c116657a173ceaa34fbca15a924b7a9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "installer/installer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "installer/installer.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "installer/installer.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "DTS SFTP";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    
<img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("DTSlogo.png", "logo"), "html", null, true);
        echo "\"  width=\"250px\"/>
<h4 class=\"title\">SFTP Client Post Installation Settings</h4>
<h4 class=\"text-blue\" >Please complete fields bellow</h4>
    <form action=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("generate_script");
        echo "\" method=\"post\" onsubmit=\"return validacion()\">  
        <p class=\"margin\">Credentials</p>
            <p>User:</p>
            <input class=\"field\" type=\"text\" id=\"user\" name=\"user\" minlength=\"3\" maxlength=\"20\" size=\"15\" required>
                
            <p>Password:</p>
            <input class=\"field\" type=\"text\" id=\"password\" name=\"password\" minlength=\"3\" maxlength=\"20\" size=\"15\" required>
            <button type=\"button\" id=\"btnConnect\" name=\"button\" class=\"btn2\" >Connect</button>
            <div id=\"gif\" class=\"gif\"></div>

            <input class=\"field\" type=\"hidden\" id=\"jsonTo\" name=\"jsonTo\">
            <input class=\"field\" type=\"hidden\" id=\"jsonFrom\" name=\"jsonFrom\">
        
        <div id=\"container-form\" class=\"container-form\">    
            <p class=\"margin2\">Local SFTP directories</p>
                <p>SFTP client download-to directory</p>
                <input class=\"field\" type=\"text\" id=\"fromdts\" name=\"fromdts\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["fromDTS"]) || array_key_exists("fromDTS", $context) ? $context["fromDTS"] : (function () { throw new RuntimeError('Variable "fromDTS" does not exist.', 26, $this->source); })()), "html", null, true);
        echo "\" readonly>

                <p>SFTP client upload-from directory</p>
                <input class=\"field\" type=\"text\" id=\"todts\" name=\"todts\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["toDTS"]) || array_key_exists("toDTS", $context) ? $context["toDTS"] : (function () { throw new RuntimeError('Variable "toDTS" does not exist.', 29, $this->source); })()), "html", null, true);
        echo "\" readonly>

                <p>SFTP client archive directory</p>
                <input class=\"field\" type=\"text\" id=\"backup\" name=\"backup\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["backup"]) || array_key_exists("backup", $context) ? $context["backup"] : (function () { throw new RuntimeError('Variable "backup" does not exist.', 32, $this->source); })()), "html", null, true);
        echo "\" readonly>

                <p>SFTP client temporary directory</p>
                <input class=\"field\" type=\"text\" id=\"temp\" name=\"temp\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["temp"]) || array_key_exists("temp", $context) ? $context["temp"] : (function () { throw new RuntimeError('Variable "temp" does not exist.', 35, $this->source); })()), "html", null, true);
        echo "\" readonly>
                <input class=\"field\" type=\"hidden\" id=\"pathlocal\" name=\"pathlocal\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 36, $this->source); })()), "html", null, true);
        echo "\">
                <input class=\"field\" type=\"hidden\" id=\"pathInstaller\" name=\"pathInstaller\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["pathInstaller"]) || array_key_exists("pathInstaller", $context) ? $context["pathInstaller"] : (function () { throw new RuntimeError('Variable "pathInstaller" does not exist.', 37, $this->source); })()), "html", null, true);
        echo "\">
               <br><br>

            <label class=\"margin2\">Upload subdirectories?</label>
            <input class=\"inputCheckbox\" type=\"checkbox\" id=\"subDirs\" name=\"subDirs\" onclick=\"showInputs('local');\">
           

            <div id=\"divqty\" style=\"display: none\">

                <label>Specify numbers of sub-directories:</label>
                <input class=\"field2\" type=\"number\" id=\"qtySubdirs\" name=\"qtySubdirs\" min=\"1\" max=\"12\" size=\"1\" onblur=\"CreateInputs('local')\">

            </div>

            <div id=\"LocalSubDir\" style=\"width:900px;\">
                <div id=\"localFrom0\" style=\"width:450px; float:left;\">
                    <div id=\"localFromdir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"localFrom\" style=\"width:200px; float:right;\"></div>
                </div>
                <div id=\"remoteTo0\" style=\"width:450px; float:left;\">
                    <div id=\"remoteTodir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"remoteTo\" style=\"width:200px; float:right;\"></div>
                </div>
            </div>
        
            <br><br>
        
            <div id=\"label\">
            <label class=\"margin2\" >Download sub-directories?</label>
            <input class=\"inputCheckbox\" type=\"checkbox\" id=\"subDirsRemote\" name=\"subDirsRemote\" onclick=\"showInputs('remote');\">
            </div>
            <div id=\"divqtyremote\" style=\"display: none\">

              <label>Specify numbers of sub-directories</label>
              <input class=\"field2\" type=\"number\" id=\"qtySubdirsRemote\" name=\"qtySubdirsRemote\" min=\"1\" max=\"12\" size=\"1\" onblur=\"CreateInputs('remote')\">

            </div>

            <div id=\"RemoteSubDir\" style=\"width:900px;\">
                <div id=\"remoteFrom0\" style=\"width:450px; float:left;\">
                    <div id=\"remoteFromdir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"remoteFrom\" style=\"width:200px; float:right;\"></div>
                </div>
                <div id=\"remoteTo0\" style=\"width:450px; float:left;\">
                    <div id=\"localTodir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"localTo\" style=\"width:200px; float:right;\"></div>
                </div>
            </div>
            
            <div id=\"label2\">
               <button type=\"submit\" class=\"btn\" id=\"btnDownload\">Script Download</button>
            </div>
        </div>
    </form>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 93
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 94
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script src=";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/validations.js"), "html", null, true);
        echo "></script>
    <script src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 97
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_js_routing_js", ["callback" => "fos.Router.setData"]);
        echo "\"></script>
    
    <script>
    \$(function () {
        \$('#btnConnect').click(function () {
            
            if( \$('#user').val() == \"\"  || /^\\s+\$/.test(\$('#user').val()) ) {
                alert (\"User is required\");
                return false;
            }
            if( \$('#password').val() == \"\" || /^\\s+\$/.test(\$('#password').val())) {
                alert (\"Password is required.\")
                return false;
            }
            if (\$('#user').val() !== '' && \$('#password').val() !== '') {
                loadFolder(\$('#user').val(),\$('#password').val());
            }
        });        
    });

    function loadFolder(user, password){

       \$.ajax({
            method: 'POST',
            url: Routing.generate('loadfolder', {user: user, password: password}),
            beforeSend: function () {
                \$(\"#container-form\").css({'display':'none'});
                \$(\"#gif\").css({'display':'block'}); 
                \$('#gif').html(\$('#gif').html() + '<img src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/ajax-loader.gif"), "html", null, true);
        echo "\" alt=\"Conecting...\">');
            },
            success: function (result) {
                \$(\"#container-form\").css({'display':'block'});
                \$('#gif').html(\"\");
                \$(\"#gif\").css({'display':'none'});
                \$.each(result, function(i, item) {
                     \$('#json'+i).val(item);
                });
            },

            error : function(xhr, status) {
                \$('#gif').html(\"\");
                \$(\"#container-form\").css({'display':'none'});
                \$(\"#gif\").css({'display':'none'});
                alert('Cannot login into server, verify credentials');
            },

        });
    }
    </script>
 
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "installer/installer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 125,  230 => 97,  226 => 96,  222 => 95,  219 => 94,  209 => 93,  143 => 37,  139 => 36,  135 => 35,  129 => 32,  123 => 29,  117 => 26,  98 => 10,  92 => 7,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}DTS SFTP{% endblock %}

{% block body %}
    
<img src=\"{{ asset('DTSlogo.png', 'logo') }}\"  width=\"250px\"/>
<h4 class=\"title\">SFTP Client Post Installation Settings</h4>
<h4 class=\"text-blue\" >Please complete fields bellow</h4>
    <form action=\"{{path('generate_script')}}\" method=\"post\" onsubmit=\"return validacion()\">  
        <p class=\"margin\">Credentials</p>
            <p>User:</p>
            <input class=\"field\" type=\"text\" id=\"user\" name=\"user\" minlength=\"3\" maxlength=\"20\" size=\"15\" required>
                
            <p>Password:</p>
            <input class=\"field\" type=\"text\" id=\"password\" name=\"password\" minlength=\"3\" maxlength=\"20\" size=\"15\" required>
            <button type=\"button\" id=\"btnConnect\" name=\"button\" class=\"btn2\" >Connect</button>
            <div id=\"gif\" class=\"gif\"></div>

            <input class=\"field\" type=\"hidden\" id=\"jsonTo\" name=\"jsonTo\">
            <input class=\"field\" type=\"hidden\" id=\"jsonFrom\" name=\"jsonFrom\">
        
        <div id=\"container-form\" class=\"container-form\">    
            <p class=\"margin2\">Local SFTP directories</p>
                <p>SFTP client download-to directory</p>
                <input class=\"field\" type=\"text\" id=\"fromdts\" name=\"fromdts\" value=\"{{fromDTS}}\" readonly>

                <p>SFTP client upload-from directory</p>
                <input class=\"field\" type=\"text\" id=\"todts\" name=\"todts\" value=\"{{toDTS}}\" readonly>

                <p>SFTP client archive directory</p>
                <input class=\"field\" type=\"text\" id=\"backup\" name=\"backup\" value=\"{{backup}}\" readonly>

                <p>SFTP client temporary directory</p>
                <input class=\"field\" type=\"text\" id=\"temp\" name=\"temp\" value=\"{{temp}}\" readonly>
                <input class=\"field\" type=\"hidden\" id=\"pathlocal\" name=\"pathlocal\" value=\"{{path}}\">
                <input class=\"field\" type=\"hidden\" id=\"pathInstaller\" name=\"pathInstaller\" value=\"{{pathInstaller}}\">
               <br><br>

            <label class=\"margin2\">Upload subdirectories?</label>
            <input class=\"inputCheckbox\" type=\"checkbox\" id=\"subDirs\" name=\"subDirs\" onclick=\"showInputs('local');\">
           

            <div id=\"divqty\" style=\"display: none\">

                <label>Specify numbers of sub-directories:</label>
                <input class=\"field2\" type=\"number\" id=\"qtySubdirs\" name=\"qtySubdirs\" min=\"1\" max=\"12\" size=\"1\" onblur=\"CreateInputs('local')\">

            </div>

            <div id=\"LocalSubDir\" style=\"width:900px;\">
                <div id=\"localFrom0\" style=\"width:450px; float:left;\">
                    <div id=\"localFromdir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"localFrom\" style=\"width:200px; float:right;\"></div>
                </div>
                <div id=\"remoteTo0\" style=\"width:450px; float:left;\">
                    <div id=\"remoteTodir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"remoteTo\" style=\"width:200px; float:right;\"></div>
                </div>
            </div>
        
            <br><br>
        
            <div id=\"label\">
            <label class=\"margin2\" >Download sub-directories?</label>
            <input class=\"inputCheckbox\" type=\"checkbox\" id=\"subDirsRemote\" name=\"subDirsRemote\" onclick=\"showInputs('remote');\">
            </div>
            <div id=\"divqtyremote\" style=\"display: none\">

              <label>Specify numbers of sub-directories</label>
              <input class=\"field2\" type=\"number\" id=\"qtySubdirsRemote\" name=\"qtySubdirsRemote\" min=\"1\" max=\"12\" size=\"1\" onblur=\"CreateInputs('remote')\">

            </div>

            <div id=\"RemoteSubDir\" style=\"width:900px;\">
                <div id=\"remoteFrom0\" style=\"width:450px; float:left;\">
                    <div id=\"remoteFromdir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"remoteFrom\" style=\"width:200px; float:right;\"></div>
                </div>
                <div id=\"remoteTo0\" style=\"width:450px; float:left;\">
                    <div id=\"localTodir\" style=\"width:250px; float:left;\"></div>
                    <div id=\"localTo\" style=\"width:200px; float:right;\"></div>
                </div>
            </div>
            
            <div id=\"label2\">
               <button type=\"submit\" class=\"btn\" id=\"btnDownload\">Script Download</button>
            </div>
        </div>
    </form>

{% endblock %}
{% block javascripts %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script src={{asset('js/validations.js')}}></script>
    <script src=\"{{ asset('bundles/fosjsrouting/js/router.js') }}\"></script>
    <script src=\"{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}\"></script>
    
    <script>
    \$(function () {
        \$('#btnConnect').click(function () {
            
            if( \$('#user').val() == \"\"  || /^\\s+\$/.test(\$('#user').val()) ) {
                alert (\"User is required\");
                return false;
            }
            if( \$('#password').val() == \"\" || /^\\s+\$/.test(\$('#password').val())) {
                alert (\"Password is required.\")
                return false;
            }
            if (\$('#user').val() !== '' && \$('#password').val() !== '') {
                loadFolder(\$('#user').val(),\$('#password').val());
            }
        });        
    });

    function loadFolder(user, password){

       \$.ajax({
            method: 'POST',
            url: Routing.generate('loadfolder', {user: user, password: password}),
            beforeSend: function () {
                \$(\"#container-form\").css({'display':'none'});
                \$(\"#gif\").css({'display':'block'}); 
                \$('#gif').html(\$('#gif').html() + '<img src=\"{{ asset('images/ajax-loader.gif') }}\" alt=\"Conecting...\">');
            },
            success: function (result) {
                \$(\"#container-form\").css({'display':'block'});
                \$('#gif').html(\"\");
                \$(\"#gif\").css({'display':'none'});
                \$.each(result, function(i, item) {
                     \$('#json'+i).val(item);
                });
            },

            error : function(xhr, status) {
                \$('#gif').html(\"\");
                \$(\"#container-form\").css({'display':'none'});
                \$(\"#gif\").css({'display':'none'});
                alert('Cannot login into server, verify credentials');
            },

        });
    }
    </script>
 
{% endblock %}", "installer/installer.html.twig", "/var/www/html/sftp/templates/installer/installer.html.twig");
    }
}
