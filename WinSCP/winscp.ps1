 Param( 
            [parameter(mandatory=$true)][string]$file 
        )



function Start-Main{
    if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }    
    
       Filetransfer-WinSCP

}
        
function Filetransfer-WinSCP{
    try{


        Get-Content "$file" | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,'='); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }
      

        Add-Type -Path "C:\WinSCP\WinSCPnet.dll"
        $ServerName=$h.Get_Item("hostname")
        $Username=$h.Get_Item("username")
        $password=$h.Get_Item("password")
        $fingerprint=$h.Get_Item("fingerprint")
        $ftp_log=$h.Get_Item("ftp_log")
        $application_log=$h.Get_Item("application_log")
        $inbound_source=$h.Get_Item("inbound_source")
        $inbound_target=$h.Get_Item("inbound_target")
		$inbound_filename=$h.Get_Item("inbound_filename")
        $inbound_backup=$h.Get_Item("inbound_backup")

        $sessionOptions = New-Object WinSCP.SessionOptions -Property @{ 
            Protocol = [WinSCP.Protocol]::Sftp
            HostName = $ServerName
            UserName = $Username
            Password = $password			
			GiveUpSecurityAndAcceptAnySshHostKey = $True
            #SshHostKeyFingerprint = $fingerprint 
        }
		#$sessionOptions.SshHostKeyPolicy = [WinSCP.SshHostKeyPolicy]::AcceptNew
        $session = New-Object WinSCP.Session
        $session.SessionLogPath=$ftp_log
        $session.DebugLogPath=$application_log
        $session.Open($sessionOptions)
        $transferOptions = New-Object WinSCP.TransferOptions
        $transferOptions.TransferMode = [WinSCP.TransferMode]::Binary
        
               
        $SourcePath=$inbound_source + $inbound_filename
        $DestinationPath=$inbound_target
        if (-not (test-path $DestinationPath)) 
        {
            New-Item $DestinationPath -itemType Directory
        }
        $transferResult=$session.GetFiles("$SourcePath", "$DestinationPath", $False, $transferOptions)
		foreach ($transfer in $transferResult.Transfers)
        {
            if ($transfer.Error -eq $Null)
            {
				if($inbound_backup -eq $Null) 
				{
					Write-Host "Deleted inbound file: $($transfer.FileName)"
					$session.RemoveFile($transfer.FileName)
				}
				else
				{
					if (-not ($session.FileExists($inbound_backup)))
					{
						Write-Host "Archive directory $inbound_backup not found, creating it"
						$session.CreateDirectory($inbound_backup)
					}
					$newName =$inbound_backup+[IO.Path]::GetFileName($transfer.FileName)+"_"+(Get-Date -Format "yyyyMMddhhmmss")
					$session.MoveFile($transfer.FileName,$newName)
					Write-Host "Moved inbound file: $($transfer.FileName) to $fileNameBackup $outbound_backup"
				}
                
				
            }
        }
        $outbound_source=$h.Get_Item("outbound_source")
		$outbound_filename=$h.Get_Item("outbound_filename")
        $outbound_target=$h.Get_Item("outbound_target")
        $outbound_backup=$h.Get_Item("outbound_backup")

        if (-not (test-path $outbound_source)) 
        {
            New-Item $outbound_source -itemType Directory
        }
		
        $SourcePath=$outbound_source + $outbound_filename    
        $DestinationPath=$outbound_target
        $transferResult=$session.PutFiles("$SourcePath", "$DestinationPath", $False, $transferOptions)
		foreach ($transfer in $transferResult.Transfers)
        {
            if ($transfer.Error -eq $Null)
            {
				if($outbound_backup -eq $Null) 
				{
					Remove-item $transfer.FileName
					Write-Host "Deleted outbound file: $($transfer.FileName)"
				}
				else
				{
					if (-not (test-path $outbound_backup)) 
					{
						Write-Host "Archive directory $outbound_backup not found, creating it"
						New-Item $outbound_backup -itemType Directory
					}
					$fileNameBackup=$transfer.FileName+"_"+(Get-Date -Format "yyyyMMddhhmmss")
					Rename-item $transfer.FileName $fileNameBackup 
					Move-item $fileNameBackup $outbound_backup
					
					Write-Host "Deleted outbound file: $($transfer.FileName) to $fileNameBackup $outbound_backup"
				}
				
					
            }
        }
    }
	catch
    {
        Write-Host "Error: $($_Exception.Message)"
        exit 1
    }
    finally
    {
        $session.Dispose()
    }   
	exit 0	
    
	
}

Start-Main


